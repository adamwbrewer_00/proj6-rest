# Laptop Service
from flask import Flask, jsonify, Response, request
from flask_restful import Resource, Api
from pymongo import MongoClient
import pymongo
import logging
import csv

# Instantiate the app
app = Flask(__name__)
# error handlinng with flask.jsonify and dictionaries
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
api = Api(app)

client = MongoClient('db', 27017)
db = client.tododb

"""
This function takes items(data) submitted into MongoDB
then sorts the data by km and index. (index created in DockerMongo/app.py)
After it takes the sorted data and constructs
a large dictionary based on sample-data.json.
Structure:
{'brevets':list of brevet dictionaries for each brevet submitted}
Takes arguments open_bool and close_bool as Booleans
to tell if open and close times are wanted.
"""
def format_dict(items, open_bool, close_bool):
    sort_items = sorted(items, key=lambda d: int(d['km'])) # sort items
    sort_items = sorted(sort_items, key=lambda d: int(d['index'])) # sort items

    brevets = []
    for partial_dict in sort_items:
        controls = {}

        if(brevets == []):         # if brevets empty
            controls["km"] = float(partial_dict['km'])
            controls["mi"] = float(partial_dict['mi'])
            if(open_bool == True):
                controls["open_time"] = partial_dict['open_time']
            if(close_bool == True):
                controls["close_time"] = partial_dict['close_time']
            del partial_dict["km"]
            del partial_dict["mi"]
            del partial_dict["open_time"]
            del partial_dict["close_time"]
            del partial_dict["_id"]
            del partial_dict["index"]
            partial_dict["controls"] = [controls]
            brevets.append(partial_dict)

        elif(partial_dict['index'] != (len(brevets)-1)): # if new brevet ntered
            controls["km"] = float(partial_dict['km'])
            controls["mi"] = float(partial_dict['mi'])
            if(open_bool == True):
                controls["open_time"] = partial_dict['open_time']
            if(close_bool == True):
                controls["close_time"] = partial_dict['close_time']
            del partial_dict["km"]
            del partial_dict["mi"]
            del partial_dict["open_time"]
            del partial_dict["close_time"]
            del partial_dict["_id"]
            del partial_dict["index"]
            partial_dict["controls"] = [controls]
            brevets.append(partial_dict)

        else:  # if brevet has more than one control point
            for i in range(len(brevets)):
                mini_dict = {}
                if(partial_dict['index'] == i): # if partial_dict holds close & open time for brevet[i]
                    mini_dict["km"] = float(partial_dict['km'])
                    mini_dict["mi"] = float(partial_dict['mi'])

                    if(open_bool == True):
                        mini_dict["open_time"] = partial_dict['open_time']

                    if(close_bool == True):
                        mini_dict["close_time"] = partial_dict['close_time']

                    brevets[i]["controls"].append(mini_dict)

    # Turns list of brevet dictionaries into latge dictionary
    data_dict = {}
    data_dict["brevets"] = brevets
    app.logger.debug("All Data: {}".format(data_dict))
    return data_dict

"""
This function takes data_dict (created dictionary from format_dict())
and decontructs it into headers and data.
Headers are made up of all the keys and subkeys from the dictionary.
Data is made from all the values the dictionaries hold.
Then the headers and data are written to a blank csv file named
'data.csv' which is created and updated whenever the csvify function is called.
This does not return anything as we are writing on a file instead.
"""
def csvify(data_dict):
    brevets = data_dict['brevets']
    headers = []
    data = []
    header_cap = 0
    keys = [item for item in data_dict]
    for i in keys:
        inner_dict = [item for item in data_dict[i]] # finds brevet dictionary
        for j in inner_dict:

            if(header_cap == 0): # only first brevet dictionary used for keys & subkeys
                # finds all headers
                for k in j:
                    if(str(k) == 'controls'):
                        controls = j['controls']
                        for q in range(len(controls)):
                            control_dict = controls[q]
                            for p in control_dict:
                                headers.append(str(i)+'/'+str(k)+'/'+str(q)+'/'+str(p))  # writes keys in contols
                        break # stops from inculing excess information
                    else:
                        headers.append(str(i) + '/' + str(k)) # writes keys of inner dicts (begin_time,distance,...)
                header_cap += 1

            # finds all data
            mini_data = []
            for k in j:
                if(str(k) == 'controls'):
                    controls = j['controls']
                    for q in range(len(controls)):
                        control_dict = controls[q]
                        for p in control_dict:
                            mini_data.append(control_dict[str(p)])  # writes keys in contols
                else:
                    mini_data.append(j[str(k)]) # writes keys of inner dicts (begin_time,distance,...)
            data.append(mini_data)

    # writes headers and data to 'data.csv'
    with open('data.csv', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        # write the header
        writer.writerow(headers)
        # write multiple rows
        writer.writerows(data)

# Funtions to expose Restful services
# First three basic api functions
class listAll(Resource):
    def get(self):
        # request top argument from url
        top = request.args.get("top")
        # sets top to default if no argument found
        if top == None:
            top = 20

        _items = db.tododb.find() # Gets data from MongoDB
        _items = _items.limit(int(top)) # Limits data amount = top
        items = [item for item in _items]
        app.logger.debug("Item stored: {}".format(items))
        # returns data in dictionary format with Open & Close times
        return format_dict(items,True,True)

class listOpenOnly(Resource):
    def get(self):
        # request top argument from url
        top = request.args.get("top")
        # sets top to default if no argument found
        if top == None:
            top = 20

        _items = db.tododb.find() # Gets data from MongoDB
        _items = _items.limit(int(top)) # Limits data amount = top
        items = [item for item in _items]
        app.logger.debug("Item stored: {}".format(items))
        # returns data in dictionary format with Open times
        return format_dict(items,True,False)

class listCloseOnly(Resource):
    def get(self):
        # request top argument from url
        top = request.args.get("top")
        # sets top to default if no argument found
        if top == None:
            top = 20

        _items = db.tododb.find() # Gets data from MongoDB
        _items = _items.limit(int(top)) # Limits data amount = top
        items = [item for item in _items]
        app.logger.debug("Item stored: {}".format(items))
        # returns data in dictionary format with Close times
        return format_dict(items,False,True)


# Three Api functons for csv
class listAllcsv(Resource):
    def get(self):
        # request top argument from url
        top = request.args.get("top")
        # sets top to default if no argument found
        if top == None:
            top = 20

        _items = db.tododb.find() # Gets data from MongoDB
        _items = _items.limit(int(top)) # Limits data amount = top
        items = [item for item in _items]
        app.logger.debug("Item stored: {}".format(items))
        # returns data in dictionary format with Open & Close times
        data_dict = format_dict(items,True,True)
        # turns dictionary into csv file
        csvify(data_dict)
        csv_file = open('data.csv', 'r')
        return Response(csv_file, mimetype='text/csv')

class listOpenOnlycsv(Resource):
    def get(self):
        # request top argument from url
        top = request.args.get("top")
        # sets top to default if no argument found
        if top == None:
            top = 20

        _items = db.tododb.find() # Gets data from MongoDB
        _items = _items.limit(int(top)) # Limits data amount = top
        items = [item for item in _items]
        app.logger.debug("Item stored: {}".format(items))
        # returns data in dictionary format with Open times
        data_dict = format_dict(items,True,False)
        # turns dictionary into csv file
        csvify(data_dict)
        csv_file = open('data.csv', 'r')
        return Response(csv_file, mimetype='text/csv')

class listCloseOnlycsv(Resource):
    def get(self):
        # request top argument from url
        top = request.args.get("top")
        # sets top to default if no argument found
        if top == None:
            top = 20

        _items = db.tododb.find() # Gets data from MongoDB
        _items = _items.limit(int(top)) # Limits data amount = top
        items = [item for item in _items]
        app.logger.debug("Item stored: {}".format(items))
        # returns data in dictionary format with Close times
        data_dict = format_dict(items,False,True)
        # turns dictionary into csv file
        csvify(data_dict)
        csv_file = open('data.csv', 'r')
        return Response(csv_file, mimetype='text/csv')


# Create routes
# Add all resources in this section of code
api.add_resource(listAll,'/listAll','/listAll/json')
api.add_resource(listOpenOnly,'/listOpenOnly','/listOpenOnly/json')
api.add_resource(listCloseOnly,'/listCloseOnly','/listCloseOnly/json')
api.add_resource(listAllcsv, '/listAll/csv')
api.add_resource(listOpenOnlycsv, '/listOpenOnly/csv')
api.add_resource(listCloseOnlycsv, '/listCloseOnly/csv')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
