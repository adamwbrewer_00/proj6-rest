<html>
    <head>
        <title>CIS 322 REST-api</title>
    </head>

    <body>

# all uri commands - - - - - - - - - - - - - - - - - - - - - -

        <h1>/List All</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
            $brevets = $obj->brevets; # Get brevets dictionary
            if ($brevets == [])       # Check in dictionary is empty
            {
                echo "Database is empty";
            } else {
                foreach($brevets as $k) # decontructs dictionary
                {
                    $distance = $k->distance;
                    $begin_date = $k->begin_date;
                    $begin_time = $k->begin_time;
                    $controls = $k->controls;
                    echo "** Brevet **<br>";
                    echo "distance: $distance<br>";
                    echo "begin_date:$begin_date<br>";
                    echo "begin_time:$begin_time<br>";
                    echo "<br>";
                    echo "* Contol Points *<br>";

                    foreach($controls as $l)
                    {
                        $open_time = $l->open_time;
                        $close_time = $l->close_time;
                        $km = $l->km;
                        $mi = $l->mi;

                        echo "km: $km<br>";
                        echo "mi: $mi<br>";
                        echo "Open time: $open_time<br>";
                        echo "Close time: $close_time<br>";
                        echo "<br>";
                    }
                    echo "---------------------<br>";  # If more that one brevet in data this helps understand the data
                    echo "<br>";
                }
              }
            ?>

        <h1>/List Open Only</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($json);
            $brevets = $obj->brevets; # gets brevet dictionary
            if ($brevets == [])       # checks if dictionary empty
            {
                echo "Database is empty";
            } else {
                foreach($brevets as $k) # decontructs dictionary
                {
                    $distance = $k->distance;
                    $begin_date = $k->begin_date;
                    $begin_time = $k->begin_time;
                    $controls = $k->controls;
                    echo "** Brevet ** <br>";
                    echo "distance: $distance<br>";
                    echo "begin_date:$begin_date<br>";
                    echo "begin_time:$begin_time<br>";
                    echo "<br>";
                    echo "* Contol Points * <br>";

                    foreach($controls as $l)
                    {
                        $open_time = $l->open_time;
                        $km = $l->km;
                        $mi = $l->mi;

                        echo "km: $km<br>";
                        echo "mi: $mi<br>";
                        echo "Open time: $open_time<br>";
                        echo "<br>";
                    }
                    echo "---------------------<br>";
                    echo "<br>";
                }
              }
            ?>

        <h1>/List Close Only</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly');
            $obj = json_decode($json);
            $brevets = $obj->brevets; # gets brevets dictionary
            if ($brevets == [])       # checks if dictionary empty
            {
                echo "Database is empty";
            } else {
                foreach($brevets as $k)
                {
                    $distance = $k->distance;
                    $begin_date = $k->begin_date;
                    $begin_time = $k->begin_time;
                    $controls = $k->controls;
                    echo "** Brevet **<br>";
                    echo "distance: $distance<br>";
                    echo "begin_date:$begin_date<br>";
                    echo "begin_time:$begin_time<br>";
                    echo "<br>";
                    echo "* Contol Points *<br>";

                    foreach($controls as $l)
                    {
                        $close_time = $l->close_time;
                        $km = $l->km;
                        $mi = $l->mi;

                        echo "km: $km<br>";
                        echo "mi: $mi<br>";
                        echo "Close time: $close_time<br>";
                        echo "<br>";
                    }
                    echo "---------------------<br>";
                    echo "<br>";
                }
              }
            ?>

# all uri JSON commands - - - - - - - - - - - - - - - - - - - - - -

        <h1>/List All JSON</h1>
            <?php
            # Same as listAll function above
            $json = file_get_contents('http://laptop-service/listAll/json');
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            if ($brevets == [])
            {
                echo "Database is empty";
            } else {
                foreach($brevets as $k)
                {
                    $distance = $k->distance;
                    $begin_date = $k->begin_date;
                    $begin_time = $k->begin_time;
                    $controls = $k->controls;
                    echo "** Brevet **<br>";
                    echo "distance: $distance<br>";
                    echo "begin_date:$begin_date<br>";
                    echo "begin_time:$begin_time<br>";
                    echo "<br>";
                    echo "* Contol Points *<br>";

                    foreach($controls as $l)
                    {
                        $open_time = $l->open_time;
                        $close_time = $l->close_time;
                        $km = $l->km;
                        $mi = $l->mi;

                        echo "km: $km<br>";
                        echo "mi: $mi<br>";
                        echo "Open time: $open_time<br>";
                        echo "Close time: $close_time<br>";
                        echo "<br>";
                    }
                    echo "---------------------<br>";
                    echo "<br>";
                }
              }
            ?>

        <h1>/List Open Only JSON</h1>
            <?php
            # Same as listOpenOnly function above
            $json = file_get_contents('http://laptop-service/listOpenOnly/json');
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            if ($brevets == [])
            {
                echo "Database is empty";
            } else {
                foreach($brevets as $k)
                {
                    $distance = $k->distance;
                    $begin_date = $k->begin_date;
                    $begin_time = $k->begin_time;
                    $controls = $k->controls;
                    echo "** Brevet ** <br>";
                    echo "distance: $distance<br>";
                    echo "begin_date:$begin_date<br>";
                    echo "begin_time:$begin_time<br>";
                    echo "<br>";
                    echo "* Contol Points * <br>";

                    foreach($controls as $l)
                    {
                        $open_time = $l->open_time;
                        $km = $l->km;
                        $mi = $l->mi;

                        echo "km: $km<br>";
                        echo "mi: $mi<br>";
                        echo "Open time: $open_time<br>";
                        echo "<br>";
                    }
                    echo "---------------------<br>";
                    echo "<br>";
                }
              }
            ?>

        <h1>/List Close Only JSON</h1>
            <?php
            # Same as listCloseOnly function above
            $json = file_get_contents('http://laptop-service/listCloseOnly/json');
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            if ($brevets == [])
            {
                echo "Database is empty";
            } else {
                foreach($brevets as $k)
                {
                    $distance = $k->distance;
                    $begin_date = $k->begin_date;
                    $begin_time = $k->begin_time;
                    $controls = $k->controls;
                    echo "** Brevet **<br>";
                    echo "distance: $distance<br>";
                    echo "begin_date:$begin_date<br>";
                    echo "begin_time:$begin_time<br>";
                    echo "<br>";
                    echo "* Contol Points *<br>";

                    foreach($controls as $l)
                    {
                        $close_time = $l->close_time;
                        $km = $l->km;
                        $mi = $l->mi;

                        echo "km: $km<br>";
                        echo "mi: $mi<br>";
                        echo "Close time: $close_time<br>";
                        echo "<br>";
                    }
                    echo "---------------------<br>";
                    echo "<br>";
                }
              }
            ?>

# all uri CSV commands - - - - - - - - - - - - - - - - - - - - - -

        <h1>CSV  listAll </h1>
            <?php
            # look at csvify function in labtop/api.py to undsertand how this is created
             echo file_get_contents('http://laptop-service/listAll/csv');
             ?>
        <h1>CSV OpenOnly</h1>
            <?php
            # look at csvify function in labtop/api.py to undsertand how this is created
             echo file_get_contents('http://laptop-service/listOpenOnly/csv');
             ?>
        <h1>CSV CloseOnly</h1>
            <?php
            # look at csvify function in labtop/api.py to undsertand how this is created
             echo file_get_contents('http://laptop-service/listCloseOnly/csv');
             echo "<br>";
             echo "<br>"; # these help break up commands
             ?>

# all uri Top commands - - - - - - - - - - - - - - - - - - - - - -

        <h1>Top = 3 Open Time in CSV</h1>
            <?php
            # look at csvify function in labtop/api.py to undsertand how this is created
            echo file_get_contents('http://laptop-service/listOpenOnly/csv?top=3');
            ?>
        <h1>Top = 6 Close Time in CSV</h1>
            <?php
            # look at csvify function in labtop/api.py to undsertand how this is created
            echo file_get_contents('http://laptop-service/listCloseOnly/csv?top=6');
            ?>
        <h1>Top = 4 Close Time in JSON</h1>
            <?php
            # look at listCloseOnly/json function above
            $json = file_get_contents('http://laptop-service/listCloseOnly/json?top=4');
            $obj = json_decode($json);
            $brevets = $obj->brevets; # gets brevets dictionary
            if ($brevets == [])       # checks if dictionary empty
            {
                echo "Database is empty";
            } else {
                foreach($brevets as $k)
                {
                    $distance = $k->distance;
                    $begin_date = $k->begin_date;
                    $begin_time = $k->begin_time;
                    $controls = $k->controls;
                    echo "** Brevet **<br>";
                    echo "distance: $distance<br>";
                    echo "begin_date:$begin_date<br>";
                    echo "begin_time:$begin_time<br>";
                    echo "<br>";
                    echo "* Contol Points *<br>";

                    foreach($controls as $l)
                    {
                        $close_time = $l->close_time;
                        $km = $l->km;
                        $mi = $l->mi;

                        echo "km: $km<br>";
                        echo "mi: $mi<br>";
                        echo "Close time: $close_time<br>";
                        echo "<br>";
                    }
                    echo "---------------------<br>";
                    echo "<br>";
                }
              }
            ?>

        <h1>Top = 5 Open Time in JSON</h1>
            <?php
            # look at listOpenOnly/json function above
            $json = file_get_contents('http://laptop-service/listOpenOnly/json?top=5');
            $obj = json_decode($json);
            $brevets = $obj->brevets; # gets brevet dictionary
            if ($brevets == [])       # checks if dictionary empty
            {
                echo "Database is empty";
            } else {
                foreach($brevets as $k) # decontructs dictionary
                {
                    $distance = $k->distance;
                    $begin_date = $k->begin_date;
                    $begin_time = $k->begin_time;
                    $controls = $k->controls;
                    echo "** Brevet ** <br>";
                    echo "distance: $distance<br>";
                    echo "begin_date:$begin_date<br>";
                    echo "begin_time:$begin_time<br>";
                    echo "<br>";
                    echo "* Contol Points * <br>";

                    foreach($controls as $l)
                    {
                        $open_time = $l->open_time;
                        $km = $l->km;
                        $mi = $l->mi;

                        echo "km: $km<br>";
                        echo "mi: $mi<br>";
                        echo "Open time: $open_time<br>";
                        echo "<br>";
                    }
                    echo "---------------------<br>";
                    echo "<br>";
                }
              }
            ?>

    </body>
</html>
