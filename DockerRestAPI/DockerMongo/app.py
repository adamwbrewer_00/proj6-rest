from flask import Flask, redirect, url_for, request, render_template, jsonify, session
from pymongo import MongoClient, ASCENDING
import arrow
import acp_times
import config
import logging
import os
import sys
sys.path.append('../credentials.ini')

app = Flask(__name__)
# was having lots of trouble configuring key from credentials.ini
# so I stopped it from being used to allow thr program to run more smoothly
#CONFIG = config.configuration()
#app.secret_key = CONFIG.SECRET_KEY

client = MongoClient('db', 27017)
db = client.tododb

sp_index = -1

@app.route('/')
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    global sp_index
    sp_index += 1
    return render_template('calc.html')

@app.route('/todo')
def todo():
    _items = db.tododb.find()
    app.logger.debug("Erroer finder {}".format(_items))   # for debugging

    items = [item for item in _items]

    items = sorted(items, key=lambda d: int(d['km'])) # sorts based on key value in int() form
    app.logger.debug("Data list {}".format(items))   # for debugging
    km_list = [int(x['km']) for x in items]

    if(len(km_list) == 0):
        return render_template('submit_error.html')

    """
    km_max = max(km_list)
    brevet = [int(x['brev']) for x in items]
    brevet_dist_km = brevet[0]
    if((km_max - brevet_dist_km) > (brevet_dist_km*.2)): # error if control point is over 20% of the overal brevet distance
        return render_template('large_number_error.html')
    """
    return render_template('display.html', items=items)


@app.route('/new', methods=['POST'])
def new():
    global sp_index
    item_doc = {
        'index': sp_index,
        'distance': request.form['brev'],
        'begin_time': request.form['time'],
        'begin_date': request.form['date'],
        'mi': request.form['mi'],
        'km': request.form['km'],
        'open_time': request.form['open'],
        'close_time': request.form['close']
    }

    db.tododb.insert_one(item_doc)
    return "Stuff done"

# Client pressed submit with no data entered
@app.route('/no_data')
def no_data():
    return render_template('submit_error.html')


@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)

    # calls args from calc.html using flask.request
    distance = request.args.get('distance', 0, type = int)
    begin_time = request.args.get("begin_time", "", type=str)
    begin_date = request.args.get("begin_date", "", type=str)

    # create starting time in date time format
    brevet_start_time = begin_date + " " + begin_time

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, distance, brevet_start_time)
    close_time = acp_times.close_time(km, distance, brevet_start_time)
    result = {"open": open_time, "close": close_time}
    return jsonify(result=result)


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    session['linkback'] = url_for("index")
    return render_template('404.html'), 404


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
