# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## Author: Adam Brewer , abrewer2@uoregon.edu
Credits to Michal Young and Ram Durairajan for the initial version of this code.

## ACP controle times
The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator).  
Additional background information is given here (https://rusa.org/pages/rulesForRiders).  
We are essentially replacing the calculator here (https://rusa.org/octime_acp.html).

## About Brevets  
Randonneuring is long distance non-competive cycling. A cycling event for randonneurs is known as a "Brevet". "Controls" are checkpoints within a brevet. This calculator calculates the open and close times for a brevet and its controls.

## How to Run  
1. Fork proj6-rest to your own repository.  
2. Then clone this repository onto your working machine.  
3. Then launch docker on your machine.  
4. Launch `localhost:5002` on an open browser of yours.  
5. Then from a linux terminal, enter your cloned repository.  
6. Enter the DockerRestAPI folder and run the command "./run.sh" on a linux terminal to boot up the calculator.
7. Then refresh your `localhost:5002` on your browser and the calculator should be up and running.
8. Then enter your data for the brevet (controls distances, begin time, begin date). Then hit the submit button when you've entered the wanted data.
9. From here you have a few cool options:  
  a) Enter more brevet data:      
        a.1) Refresh your `localhost:5002` page and redo step 8 as many times as wanted.  
  b) Display Data:      
        b.1) Hit the display button and have a new html page pop up with all your data in a table.

## How to see data in JSON  
1. To see data in JSON format, first follow 'How to Run' steps.
2. Then open a new tab.
3. To see all JSON data enter `localhost:5001/listAll` or `localhost:5001/listAll/json` into browser  
4. To see JSON data of with only open times of controls enter `localhost:5001/listOpenOnly` or `localhost:5001/listOpenOnly/json` into browser.   
5. To see JSON data of with only close times of controls enter `localhost:5001/listCloseOnly` or `localhost:5001/listCloseOnly/json` into browser.   

## How to see data in CSV  
1. To see data in CSV format, first follow 'How to Run' steps.
2. Then open a new tab.
3. To see all CSV data enter `localhost:5001/listAll/csv` into browser  
4. To see CSV data of with only open times of controls enter `localhost:5001/listOpenOnly/csv` into browser.   
5. To see CSV data of with only close times of controls enter `localhost:5001/listCloseOnly/csv` into browser.   

## How to see collective Functionality
1. To see all functions implemented on brevet data, first follow 'How to Run' steps.
2. Then open a new tab.  
3. Enter `localhost:5000 into browser`. This will show the following functions.
      1. `http://<host:port>/listAll`
      2. `http://<host:port>/listOpenOnly`
      3. `http://<host:port>/listCloseOnly`
      4. `http://<host:port>/listAll/csv`
      5. `http://<host:port>/listOpenOnly/csv`
      6. `http://<host:port>/listCloseOnly/csv`
      7. `http://<host:port>/listAll/json`
      8. `http://<host:port>/listOpenOnly/json`
      9. `http://<host:port>/listCloseOnly/json`
      10. `http://<host:port>/listOpenOnly/csv?top=3`
      11. `http://<host:port>/listCloseOnly/csv?top=6`
      12. `http://<host:port>/listOpenOnly/json?top=5`
      13. `http://<host:port>/listCloseOnly/json?top=4`

## How to access only first number of controle points  
1. Simply add `?top=#` to the end of any of the above functions ending in `csv` or `json` (functions 4,5,6,7,8,9) where # is set to a number similar to functions 10,11,12,& 13 above.  

## How to stop and clear all data
1. To terminate process, run the command "./stop.sh" in your terminal.


## Data Samples

The sample data files ([sample-data.json](data-samples/sample-data.json), [sample-data.csv](data-samples/sample-data.csv), and [sample-data-pivoted.csv](data-samples/sample-data-pivoted.csv)) provide a suggested JSON and CSV format that you could follow for your exports.

1. JSON
```json
{
   "brevets":[
      {
         "distance":200,
         "begin_date":"12/01/2021",
         "begin_time":"18:06",
         "controls":[
            {
               "km":0,
               "mi":0,
               "location":"begin",
               "open":"12/01/2021 18:06",
               "close":"12/01/2021 19:06"
            },
            {
               "km":100,
               "mi":62,
               "location":null,
               "open":"12/01/2021 21:02",
               "close":"12/02/2021 00:46"
            },
            {
               "km":150,
               "mi":93,
               "location":"second checkpoint",
               "open":"12/01/2021 22:31",
               "close":"12/02/2021 04:06"
            },
            {
               "km":200,
               "mi":124,
               "location":"last checkpoint",
               "open":"12/01/2021 23:59",
               "close":"12/02/2021 07:36"
            }
         ]
      },
      {
         "distance":1000,
         "begin_date":"01/01/2022",
         "begin_time":"00:00",
         "controls":[
            {
               "km":0,
               "mi":0,
               "location":"begin",
               "open":"01/01/2022 00:00",
               "close":"01/01/2022 01:00"
            },
            {
               "km":1000,
               "mi":621,
               "location":"finish line",
               "open":"01/01/2022 09:05",
               "close":"01/04/2022 03:00"
            }
         ]
      }
   ]
}
```

2. CSV
```csv
brevets/distance,brevets/begin_date,brevets/begin_time,brevets/controls/0/km,brevets/controls/0/mi,brevets/controls/0/location,brevets/controls/0/open,brevets/controls/0/close,brevets/controls/1/km,brevets/controls/1/mi,brevets/controls/1/location,brevets/controls/1/open,brevets/controls/1/close,brevets/controls/2/km,brevets/controls/2/mi,brevets/controls/2/location,brevets/controls/2/open,brevets/controls/2/close,brevets/controls/3/km,brevets/controls/3/mi,brevets/controls/3/location,brevets/controls/3/open,brevets/controls/3/close
200,12/01/2021,18:06,0,0,begin,12/01/2021 18:06,12/01/2021 19:06,100,62,,12/01/2021 21:02,12/02/2021 00:46,150,93,second checkpoint,12/01/2021 22:31,12/02/2021 04:06,200,124,last checkpoint,12/01/2021 23:59,12/02/2021 07:36
1000,01/01/2022,00:00,0,0,begin,01/01/2022 00:00,01/01/2022 01:00,1000,621,finish line,01/01/2022 09:05,01/04/2022 03:00,,,,,,,,,,
```
